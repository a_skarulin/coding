public class GenericsExample {
    public static void main(String[] args) {
        GenericsDemo<String> strobject;    //create a link like GenericsDemo for String
        strobject = new GenericsDemo<>("You will give points..."); //create an object of type GenericsDemo<String>
        strobject.showTypeofnewobj(); //show datatype
        System.out.println(strobject.getNewobj()); //get value
        System.out.println("======");

        GenericsDemo<Integer> intobject;  //create a link like GenericsDemo for Integer
        intobject = new GenericsDemo<>(Integer.MAX_VALUE); //create an object of type GenericsDemo<Integer>
        intobject.showTypeofnewobj(); //show datatype
        System.out.println(intobject.getNewobj()); //get value
        System.out.println("=====");

        GenericsDemo<Boolean> boobject;   //create a link like GenericsDemo for Boolean
        boobject = new GenericsDemo<>(true);  //create an object of type GenericsDemo<Boolean>
        boobject.showTypeofnewobj();  //show datatype
        System.out.println(boobject.getNewobj());  //get value

    }

}

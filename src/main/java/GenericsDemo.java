public class GenericsDemo<T> {  //create a class GenericsDemo
    T newobj; //declare an object of type
    GenericsDemo(T object){
        newobj = object;  //give the constructor a reference to an object of type T
    }

    T getNewobj(){
        return newobj; //return object
    }

    void showTypeofnewobj(){
        System.out.println("The type of this variable is " + newobj.getClass().getSimpleName()); // show newobj type
    }

}

